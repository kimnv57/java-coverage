package java_coverage;

import java.util.ArrayList;

public class staticVariable {
	public static int defaultRowOfArray = 8;
	public static int defaultColumnOfArray = 4;
	// numLoop, testpath,hpt, testcase,EO,RO,Result,analysisProcessInString
	public static ArrayList<String[]> testSimpleLoop, testInnerLoop,
			testOuterLoop;
	public static String source;
	public static int CC;
	public static String dsk3, dsk12;
	public static String NodeRelations12, NodeRelations3;
	public static String NodeElements12, NodeElements3;
	public static String variableOfTC;
	public static String TPIndex1, TPIndex2, TPIndex3, BasisPathsIndex;
	public static String TPLuanLy1, TPLuanLy2, TPLuanLy3, BasisPathsLuanLy;
	public static String analysisProcessInString3, analysisProcessInString1,
			analysisProcessInString2, analysisProcessInStringBasisPaths;
	public static String TC1, TC2, TC3, TCLoop, TCBasisPaths;
	public static String RO1, RO2, RO3, ROLoop, ROBasisPaths;

	public static String hpt1, hpt2, hpt3, hptBasisPaths;
	public static String loopLuanLy, loopIndex, loopPoint, numOfLoopPoint;
	public static String currentLoop, analysisProcessInStringOfcurrentLoop;

	public static void reset() {
		testSimpleLoop = testInnerLoop = testOuterLoop = new ArrayList<String[]>();
		source = new String();
		analysisProcessInStringBasisPaths = hptBasisPaths = BasisPathsLuanLy = BasisPathsIndex = new String();
		CC = 0;
		analysisProcessInString3 = analysisProcessInString1 = analysisProcessInString2 = new String();
		dsk3 = new String();
		NodeRelations12 = new String();
		NodeElements12 = new String();
		NodeRelations3 = new String();
		NodeElements3 = new String();
		variableOfTC = new String();
		dsk12 = new String();
		//
		TPIndex1 = TPIndex2 = TPIndex3 = new String();
		TPLuanLy1 = TPLuanLy2 = TPLuanLy3 = new String();
		TC1 = TC2 = TC3 = TCBasisPaths = TCLoop = new String();
		RO1 = RO2 = RO3 = ROBasisPaths = ROLoop = new String();
		hpt1 = hpt2 = hpt3 = new String();
		loopLuanLy = loopIndex = loopPoint = numOfLoopPoint = new String();
		currentLoop = analysisProcessInStringOfcurrentLoop = new String();
	}

	private static int maxArraySize = 10;
	private static int canTren = 10;// default
	private static int canDuoi = -10;// default
	private static int maxLoop = 500;// default

	public static void setMaxArraySize(int maxArraySize) {
		staticVariable.maxArraySize = maxArraySize;
	}

	public static void setCanTren(int N) {
		canTren = N;
	}

	public static void setCanDuoi(int N) {
		canDuoi = N;
	}

	public static void setMaxLoop(int N) {
		maxLoop = N;
	}

	public static int getMaxLoop() {
		return maxLoop;
	}

	public static int getCanTren() {
		return canTren;
	}

	public static int getCanDuoi() {
		return canDuoi;
	}

	public static int getmaxArraySize() {
		return maxArraySize;
	}

}
