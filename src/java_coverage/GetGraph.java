package java_coverage;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Stack;

import main.JNode;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.WhileStatement;

import variable.Helper;

public class GetGraph {

	final public static int NORMAL_NODE = 0;
	final public static int IF_NODE = 1;
	final public static int FOR_NODE = 2;
	final public static int WHILE_NODE = 3;
	final public static int RETURN_NODE = 4;
	// VariableDeclarationStatement
	// ExpressionStatement
	// ReturnStatement
	char[] source;
	ArrayList<CustomNode> CustomArray;
	ArrayList<JNode> NodeArray;

	private ASTNode nodecheck;
	private int count = 0;

	public GetGraph(char[] charArray) {
		source = charArray;
		CustomArray = new ArrayList<CustomNode>();
		System.out.println("Starting create graph.");
		getCustomList();
		for (int i = 0; i < CustomArray.size(); i++) {
			System.out.println(CustomArray.get(i));
		}
		JNode current = new JNode(0, 1, 1, "Start Function", NORMAL_NODE);
		createNodeArray();
		// for(int i=0;i<NodeArray.size();i++) {
		// System.out.println(NodeArray.get(i));
		// }

	}

	private void createNodeArray() {
		JNode current = new JNode(0, 1, 1, "Start Function", NORMAL_NODE);
		NodeArray = new ArrayList<JNode>();
		NodeArray.add(current);
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		//
		int index = 1;
		int id = 0;
		Stack<Integer> stack = new Stack<Integer>();
		for (int i = 0; i < CustomArray.size(); i++) {
			CustomNode a = CustomArray.get(i);
			id = a.ID + 1;
			int level = a.Level;
			int truenode = -1;
			int falsenode = -1;
			if (a.Type == NORMAL_NODE) {
				if (map.containsKey(id)) {
					truenode=falsenode=map.get(id);
				}
				else {
					truenode = id + 1;
					falsenode = id + 1;
				}
			} else if (a.Type == IF_NODE) {
			
				truenode = id + 1;
				falsenode = id + 1 + a.NumberNextTrue;
				if(map.containsKey(id)) {
					falsenode=map.get(id);
				}
				for(int j=1;j<=a.NumberNextTrue;j++) {
					CustomNode curent = CustomArray.get(id+j);
					int id2=curent.ID+1;
					if((id2+curent.NumberNext)==(id+a.NumberNextTrue)&&curent.NumberNextTrue!=0) {
						if(curent.NumberNext==curent.NumberNextTrue) {
							if(map.containsKey(id)) {
								int des=map.get(id);
								if(!map.containsKey(id2)) map.put(id2,des);
								if(!map.containsKey(id2+curent.NumberNextTrue+1)) map.put(id2+curent.NumberNextTrue, des);
							} else {
								if(!map.containsKey(id2)) map.put(id2, id+a.NumberNext+1);
								if(!map.containsKey(id2+curent.NumberNextTrue+1)) map.put(id2+curent.NumberNextTrue, id+a.NumberNext+1);
							}
						}
						else {
							if(map.containsKey(id)) {
								int des=map.get(id);
								if(!map.containsKey(id2)) map.put(id2+curent.NumberNext,des);
								if(!map.containsKey(id2+curent.NumberNextTrue+1)) map.put(id2+curent.NumberNextTrue, des);
							} else {
								if(!map.containsKey(id2)) map.put(id2+curent.NumberNext, id+a.NumberNext+1);
								if(!map.containsKey(id2+curent.NumberNextTrue+1)) map.put(id2+curent.NumberNextTrue, id+a.NumberNext+1);
							}
						}
					}
				}
				if(!map.containsKey(id + a.NumberNextTrue)) {
					map.put(id + a.NumberNextTrue, id+a.NumberNext+1);
				}
			}
			else if(a.Type==FOR_NODE) {
				truenode=id+1;
				if(map.containsKey(id))
					falsenode=map.get(id);
				else
					falsenode=id+a.NumberNextTrue+2;
				if(!map.containsKey(id+a.NumberNextTrue+1)) map.put(id+a.NumberNextTrue+1, id);
				
			}
			else if(a.Type==WHILE_NODE) {
				if (map.containsKey(id)) {
					truenode=falsenode=map.get(id);
				}
				else {
					truenode = id + 1;
					falsenode = id + a.NumberNext+1;
				}
				
			}
			else if(a.Type==RETURN_NODE) {
				truenode=falsenode=CustomArray.size()+1;
			}
			current = new JNode(id, truenode, falsenode, a.Content, a.Type);
			NodeArray.add(current);
		}
		current = new JNode(id + 1, -1, -1, "End Function", NORMAL_NODE);
		NodeArray.add(current);
		
		//sort array
		for(int i=0;i<NodeArray.size();i++) {
			for(int j=0;j<NodeArray.size()-1;j++) {
				if(NodeArray.get(j).getId()>NodeArray.get(j+1).getId()) {
					JNode first=NodeArray.get(j);
					JNode second=NodeArray.get(j+1);
					NodeArray.set(j,second);
					NodeArray.set(j+1, first);
				}
			}
		}
	}

	public ArrayList<JNode> getNodeArray() {
		return NodeArray;
	}

	private int getTypeFromOringinType(String o) {

		if (o.equals("IfStatement"))
			return IF_NODE;
		if (o.equals("ForStatement"))
			return FOR_NODE;
		if (o.equals("WhileStatement"))
			return WHILE_NODE;
		if (o.equals("ReturnStatement"))
			return RETURN_NODE;
		return NORMAL_NODE;

	}

	public void getCustomList() {
		ASTParser parser = ASTParser.newParser(AST.JLS3);

		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(source);
		// parser.setResolveBindings(true);
		CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		cu.accept(new ASTVisitor() {
			@Override
			public boolean visit(MethodDeclaration node) {
				int mdf = node.getModifiers();
				if (!Modifier.isStatic(mdf) && Modifier.isPublic(mdf)) {
					System.out.printf("Method declare: %s\n", node.getName());
					Block block = node.getBody();
					Helper.setSourceMethod(node.toString());
					block.accept(new ASTVisitor() {
						private int level = 0;
						private int index = 0;

						@Override
						public boolean visit(WhileStatement node) {
							level++;
							return true;
						}

						@Override
						public void endVisit(WhileStatement node) {
							level--;

						}

						@Override
						public boolean visit(ForStatement node) {
							level++;
							return true;
						}

						@Override
						public void endVisit(ForStatement node) {
							level--;

						}

						@Override
						public boolean visit(IfStatement node) {
							level++;
							return true;
						}

						@Override
						public void endVisit(IfStatement node) {
							level--;
						}

						@Override
						public void preVisit(ASTNode node) {

							if (node instanceof Statement
									&& !(node instanceof Block)) {
								CustomNode current;
								while(hasIndex(index))
									index++;
								int type = getTypeFromOringinType(node
										.getClass().getSimpleName());
								int numNextTrue = getNumberStatement(node, 0);
								int numNext =getNumberStatement(node, 1);
								if (type == IF_NODE) {
									current = new CustomNode(
											index,
											IF_NODE,
											((IfStatement) node)
													.getExpression().toString(),
											level, numNextTrue,numNext-1);
									CustomArray.add(current);
									index++;
									return;
								}

								if (type == WHILE_NODE) {
									current = new CustomNode(
											index,
											WHILE_NODE,
											((WhileStatement) node)
													.getExpression().toString(),
											level, numNextTrue,numNext-1);
									CustomArray.add(current);
									index++;
									return;
								}
								if (type == FOR_NODE) {
									String forcontent= node.toString();
									String forbody=((ForStatement) node).getBody().toString();
									String Expression= forcontent.replace(forbody, "");
									Expression=Expression.substring(Expression.indexOf("(")+1);
									Expression=Expression.substring(0,Expression.lastIndexOf(")"));
									String [] Expressions= Expression.split(";");
									current = new CustomNode(index,NORMAL_NODE,Expressions[0],level, 0,0);
									CustomArray.add(current);
									index++;
									current = new CustomNode(
											index,
											FOR_NODE,Expressions[1],
											level, numNextTrue,numNext-1);
									CustomArray.add(current);
									current = new CustomNode(
											index+numNext,
											NORMAL_NODE,Expressions[2],
											level, 0,0);
									CustomArray.add(current);
									index++;
									return;
								}
								if(type == RETURN_NODE) {
									current=new CustomNode(index, RETURN_NODE,node.toString(),level, 0, 0);
									CustomArray.add(current);
									index++;
									return;
								}
								current = new CustomNode(index, NORMAL_NODE,
										node.toString(), level, 0,0);
								CustomArray.add(current);
								index++;
							}
						}

					});
					return true;

				}
				return false;
			}

		});

	}

	private int getNumberStatement(ASTNode node, int check) {

		int type = getTypeFromOringinType(node.getClass().getSimpleName());

		if (check == 0) {
			node.accept(new ASTVisitor() {
				private int rank = 0;
				@Override
				public void preVisit(ASTNode node) {
					if (node instanceof Statement || node instanceof Block) {
						rank++;
						if (rank == 2)
							nodecheck = node;
					}
				}
			});
			if (nodecheck != null)
				return getNumberStatement(nodecheck, 1);
			return -1;
		} else {
			count = 0;
			node.accept(new ASTVisitor() {

				@Override
				public void preVisit(ASTNode node) {

					if (node instanceof Statement && !(node instanceof Block)) {
						count++;
					}
				}
			});
			return count;
		}
	}
	public boolean hasIndex(int ind) {
		for(int i=0;i<CustomArray.size();i++) 
			if(CustomArray.get(i).ID==ind) return true;
		return false;
	}

	class CustomNode {
		int ID;
		int Level;
		int Type;
		int NumberNextTrue;
		int NumberNext;
		String Content;

		public CustomNode(int id, int type, String content, int level, int numtrue,int numnext) {
			ID = id;
			Type = type;
			Content = content;
			Level = level;
			NumberNextTrue = numtrue;
			NumberNext = numnext;
		}

		public String toString() {
			return String.format(
					"id: %d Node: %s    type: %d, level: %d, numbernexttrue: %d, numbernext: %d\n",
					ID, Content, Type, Level, NumberNextTrue,NumberNext);
		}
	}

}
