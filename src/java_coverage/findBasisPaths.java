package java_coverage;

import java.beans.Expression;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Find all independent paths of a CFG.
 * 
 * @author nguyenducanh
 * @version 1.0
 */
public class findBasisPaths {
	/**
	 * Cyclomatic complexity
	 */
	protected static int CC = 0;
	/**
	 * 
	 */
	protected static Map<String, ArrayList<Object>> vertexMap = new HashMap<>();
	/**
	 * contains all independent paths
	 */
	protected static ArrayList<ArrayList<String>> pathList = new ArrayList<>();

	/**
	 * Get a copy of independent paths arranged by size automatically.
	 * 
	 * @return ArrayList<ArrayList<Integer>> Object. Every independent path
	 *         starts at 0 (BatDauHam) and end at the final vertex (KetThucHam). <br/>
	 *         Ex: [[0, 1, 2, 3, 4, 5, 3, 6, 7], [0, 1, 2, 3, 6, 7]]
	 */
	public static ArrayList<ArrayList<Integer>> getBasisPaths() {
		ArrayList<ArrayList<Integer>> output = new ArrayList<>();
		for (ArrayList<String> path : pathList) {
			ArrayList<Integer> newPath = new ArrayList<>();
			for (String item : path) {
				newPath.add(Integer.parseInt(item.replace("T", "").replace("F",
						"")));
			}

			if (output.size() == 0) {
				output.add(newPath);
				continue;
			}
			int i = 0;
			while (i < output.size() && output.get(i).size() > newPath.size()) {
				i++;
			}
			output.add(i, newPath);
		}
		return output;
	}

	/**
	 * Traverse graph from the true branch of a specified vertex. <br/>
	 * For example:<br/>
	 * pathBasic = [<b style="color:red;">0, 1, 2</b>, 3F, <b
	 * style="color:red;">9, 10</b>]<br/>
	 * startVertex = 3F<br/>
	 * return = [<b style="color:red;">0, 1, 2, </b>3T, 4, 5F, 8, 3F, <b
	 * style="color:red;">9, 10</b>]<br/>
	 * 
	 * @param startVertex
	 *            Ex: 1F,3F,... (F means False)
	 * @param pathBasic
	 *            the path which contains startVertex
	 * @return new path which false branch is replaced with true branch.
	 * 
	 */
	protected static ArrayList<String> findPath(String startVertex,
			ArrayList<String> pathBasic) {
		// System.out.println(pathBasic.toString());

		ArrayList<String> firstShortestPath = new ArrayList<>();
		for (String item : pathBasic) {
			if (!item.equals(startVertex.replace("T", "F"))) {
				firstShortestPath.add(item);
			} else {
				break;
			}
		}
		firstShortestPath.add(startVertex);
		while (true) {
			int size = firstShortestPath.size();
			String latestItem = firstShortestPath.get(size - 1);
			String newItem = new String();

			if (vertexMap.containsKey(latestItem)) {
				newItem = "" + vertexMap.get(latestItem).get(0);
			} else if (vertexMap.containsKey(latestItem + "F")) {
				newItem = (String) vertexMap.get(latestItem + "F").get(0);
				firstShortestPath.set(size - 1, latestItem + "F");
			}
			if (newItem.equals("-1"))
				break;
			firstShortestPath.add(newItem);
		}
		return firstShortestPath;
	}

	/**
	 * CC,vertexMap,pathList
	 */
	protected static void reset() {
		CC = 0;
		vertexMap = new HashMap<String, ArrayList<Object>>();
		pathList = new ArrayList<>();

	}

	/**
	 * 
	 * @param dsk
	 *            .See {@link}ini(String dsk) for more details.
	 */
	public static void findBasisPaths(String dsk) {
		reset();
		ini(dsk);
		Set<String> visited = new HashSet<String>();
		pathList.add(findPath("0", new ArrayList<String>()));

		while (pathList.size() != CC)
			for (int i = 0; i < pathList.size(); i++) {
				ArrayList<String> path = pathList.get(i);
				for (String item : path)
					if (item.endsWith("F") && !visited.contains(item)) {
						visited.add(item);
						pathList.add(findPath(item.replace("F", "T"), path));
						visited.add(item.replace("F", "T"));
					}
			}
		// System.out.println(pathList.toString());
		// System.out.println(getBasicPaths());
	}

	/**
	 * 
	 * @param dsk
	 *            . For example: 0(BatDauHam),10(KetThucHam)<br/>
	 * <br/>
	 *            Ex:-1 0<br/>
	 *            0 1<br/>
	 *            1 2<br/>
	 *            2 3 8<br/>
	 *            3 4<br/>
	 *            4 5 7<br/>
	 *            5 6<br/>
	 *            6 7<br/>
	 *            7 2<br/>
	 *            8 9 11<br/>
	 *            9 10<br/>
	 *            10 -1<br/>
	 *            11 10<br/>
	 */
	protected static void ini(String dsk) {
		int soDinh = dsk.split("\n").length;
		int soCanh = -1;// bo qua canh cuoi cung trong dsk
		for (String line : dsk.split("\n")) {
			String[] itemList = line.split(" ");

			if (itemList.length == 2 || itemList[1].equals(itemList[2])) {
				ArrayList<Object> value = new ArrayList<>();
				value.add(itemList[1]);
				vertexMap.put(itemList[0], value);
				soCanh++;
			} else {
				ArrayList<Object> value1 = new ArrayList<>();
				value1.add(Integer.parseInt(itemList[1]));
				vertexMap.put(itemList[0] + "T", value1);
				//
				ArrayList<Object> value2 = new ArrayList<>();
				value2.add(itemList[2]);
				vertexMap.put(itemList[0] + "F", value2);
				//
				soCanh += 2;
			}
		}
		CC = (soCanh - soDinh + 2);
		// System.out.println(vertexMap.toString() + "\nCC="
		// + (soCanh - soDinh + 2));
	}

	public static void main(String[] args) {
		findBasisPaths
				.findBasisPaths("-1 0\n0 1\n1 2\n2 3\n3 4 9\n4 5\n5 6 8\n6 7\n7 5\n8 3\n9 10\n10 -1");
		System.out.println(findBasisPaths.getBasisPaths().toString());
		Expression e = new Expression(null	, "2+3", null);
		try {
			System.out.println(e.getArguments().toString());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Formula: M = E − N + 2P,<br/>
	 * where <br/>
	 * E = the number of edges of the graph. <br/>
	 * N = the number of nodes of the graph.<br/>
	 * P = the number of connected components.(with a function, P = 1)
	 * 
	 * @return Cyclomatic complexity.
	 */
	public static int getCC() {
		return CC;
	}
}
