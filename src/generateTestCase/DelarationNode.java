package generateTestCase;

import java.util.ArrayList;
import java.util.regex.Pattern;

import java_coverage.Utils;
import java_coverage.staticVariable;

public class DelarationNode {
	private String content;
	private ArrayList<SingleDeclaration> singleDList;
	private ArrayList<Variable> tableVar;

	public DelarationNode(String declaration, ArrayList<Variable> tableVar)
			throws Exception {
		String[] notSupported = { "extern", "struct", "union", "typedef",
				"File" };
		for (String item : notSupported)
			if (Pattern.compile("\\b" + item + "\\b").matcher(declaration)
					.find())
				throw new Exception("Not support " + item);
		// support the declaration
		content = declaration;
		this.tableVar = tableVar;
		singleDList = new ArrayList<SingleDeclaration>();
	}

	public void run() throws Exception {
		content = Utils.convertCharacterToInt(content);
		content = convertToUniqueDataType(content);
		singleDList = convertSingleNodeDeclatation(content);
		for (SingleDeclaration a : singleDList) {
			if (a.isInitialize()) {
				// TH vua khai bao vua khoi tao
				if (a.isArrayVariableDeclaration())
					a.iniValueForAssignedArrayVariable();
				else
					a.iniValueForAssignedNormalVariable();
			} else {
				// TH chi khai bao bien
				if (a.isArrayVariableDeclaration())
					a.iniValueForNotAssignedArrayVariable();
				else
					a.iniValueForNotAssignedNormalVariable();
			}
		}
	}

	public ArrayList<SingleDeclaration> getSingleNodeDeclaration() {
		return singleDList;
	}

	private ArrayList<SingleDeclaration> convertSingleNodeDeclatation(
			String content) {
		ArrayList<SingleDeclaration> singleDList = new ArrayList<SingleDeclaration>();
		int type = content.indexOf("int") == 0 ? Variable.INT : Variable.DOUBLE;
		content = content.substring(content.indexOf(" ") + 1);
		int count = 0;
		String item = "";
		content += ',';
		for (Character c : content.toCharArray()) {
			if (count == 0 && c == ',') {
				if (type == Variable.INT)
					singleDList.add(new SingleDeclaration(Variable.INT, item,
							tableVar));
				else
					singleDList.add(new SingleDeclaration(Variable.DOUBLE,
							item, tableVar));
				item = "";
				continue;
			}
			item += c;
			if (c == '{')
				count++;
			else if (c == '}')
				count--;
		}
		// System.out.print("Tap bien don:\n" + singleDList.toString());
		return singleDList;
	}

	/**
	 * 
	 * @param content
	 * @return
	 */
	private String convertToUniqueDataType(String content) {
		String[] dataTypeIntegerList = { "char", "unsigned char",
				"signed char", "int", "unsigned int", "short",
				"unsigned short", "long", "unsigned long" };
		String[] dataTypeDoubleList = { "double", "float", "long double" };
		String[] dataTypeOther = { "const", "static", "unsigned", "signed" };
		content = content.replaceAll("\\bunsigned\\b\\s", "");
		content = content.replaceAll("\\bsigned\\b\\s", "");
		for (String item : dataTypeIntegerList)
			content = content.replaceAll("\\b" + item + "\\b", "int");
		for (String item : dataTypeDoubleList)
			content = content.replaceAll("\\b" + item + "\\b", "double");
		for (String item : dataTypeOther)
			content = content.replaceAll("\\b" + item + "\\b\\s", "");
		return content;
	}

	public static void main(String[] args) throws Exception {
		ArrayList<Variable> tableVar = new ArrayList<Variable>();
		// bo test mang 1 chieu:
		// a[]={},b[]={1,2},c[3]={1,2,3,4},d[3]={1,2},e[3]={1,2,3}
		// bo test mang 2 chieu:
		DelarationNode dn = new DelarationNode(
				"a[][3]={},b[][3]={{}},c[][3]={{},{},{1,2}},d[2][2]={{1},{1,2},{1,2,3}},e[2][2]={{1,2,3},{1}}",
				tableVar);
		dn.run();
		System.out.println(dn.getSingleNodeDeclaration().toString());
		System.out.println(tableVar.toString());

	}
}

class SingleDeclaration {

	private int type;
	private String rest;
	private ArrayList<Variable> tableVar;

	public SingleDeclaration(int type, String rest, ArrayList<Variable> tableVar) {
		this.type = type;
		this.rest = rest;
		this.tableVar = tableVar;
	}

	public boolean isInitialize() {
		return rest.contains("=");
	}

	public boolean isArrayVariableDeclaration() {
		if (!rest.contains("="))
			return rest.contains("[");
		return rest.substring(0, rest.indexOf("=")).contains("[");
	}

	public void iniValueForAssignedNormalVariable() throws Exception {
		Variable v = new Variable(rest.substring(0, rest.indexOf("=")), "0",
				type);
		tableVar.add(v);
		AssignmentNode an = new AssignmentNode(rest, tableVar);
		an.run();
	}

	public void iniValueForNotAssignedNormalVariable() {
		tableVar.add(new Variable(rest, rest, type));
	}

	public void iniValueForAssignedArrayVariable() throws Exception {
		String phanKhaiBao = rest.substring(0, rest.indexOf("="));
		String phanGan = rest.substring(rest.indexOf("=") + 1);
		int dimension = Utils.getDimensionOfArrayItem(phanKhaiBao);
		String nameVar = Utils.getNameOfArrayItem(phanKhaiBao);
		Variable parentVar = new Variable(nameVar, nameVar, type);
		parentVar.isArray = true;
		tableVar.add(parentVar);
		// neu khai bao mang hai chieu
		if (dimension == 2) {
			String[] s = null;
			// neu khoi tao nhieu hang
			if (rest.contains("},{"))
				s = rest.substring(rest.indexOf("{{") + 2,
						rest.lastIndexOf("}}")).split("\\},\\{");
			else if (rest.contains("{{") && rest.contains("}}"))
				// chi khoi tao mot hang duy nhat
				s = new String[] { rest.substring(rest.indexOf("{{") + 2,
						rest.lastIndexOf("}}")) };
			
			String row = Utils.getRowOfTwoDimensionArrayItem(phanKhaiBao);
			String column = Utils.getColumnOfTwoDimensionArrayItem(phanKhaiBao);
			if (s == null) {
			} else
			// Nếu mảng không có chỉ số cột
			if (column == null || column.length() == 0) {
				// do nothing
			} else {
				int iniCol = Utils.isNumber(column) ? Utils.toInt(column)
						: staticVariable.defaultColumnOfArray;
				int iniRow = 0;
				if (row == null || row.length() == 0)
					iniRow = s.length;
				else
					iniRow = Utils.isNumber(row) ? Utils.toInt(row)
							: staticVariable.defaultRowOfArray;
				for (int i = 0; i < iniRow; i++) {
					String[] num = Utils.split(s[i], ",");
					for (int j = 0; j < iniCol; j++) {
						String value = null;
						// Nếu là phần tử được gán giá trị
						if (num == null || j >= num.length)
							// Nếu là phần tử chưa được gán giá trị
							value = "0";
						else if (j < num.length)
							value = num[j];
						parentVar.add(new Variable(nameVar + "[" + i + "][" + j
								+ "]", value, type));
					}
				}
			}
		}

		else
		// neu khai bao mang mot chieu
		if (dimension == 1) {
			String row = Utils.getRowOfOneDimensionArrayItem(phanKhaiBao);
			String[] listValue = Utils.split(
					rest.substring(rest.indexOf("=") + 1).replace("{", "")
							.replace("}", ""), ",");
			// Nếu là phép gán rỗng. Ex: a[]={}
			if (listValue == null || listValue.length == 0) {
			} else
			// neu khởi tạo mảng không có chỉ số. Ex: int a[];
			if (row == null || row.length() == 0) {
				for (int j = 0; j < listValue.length; j++)
					parentVar.add(new Variable(nameVar + "[" + j + "]",
							listValue[j], type));
			} else
			// Nếu mảng có chỉ số hàng
			{
				// Nếu còn vài phần tử mảng chưa được gán
				if (Utils.toInt(row) > listValue.length) {
					for (int j = 0; j < listValue.length; j++)
						parentVar.add(new Variable(nameVar + "[" + j + "]",
								listValue[j], type));
					for (int j = listValue.length; j < Utils.toInt(row); j++)
						parentVar.add(new Variable(nameVar + "[" + j + "]",
								"0", type));

				} else
				// Nếu gán thừa vài phần tử
				{
					for (int j = 0; j < Utils.toInt(row); j++)
						parentVar.add(new Variable(nameVar + "[" + j + "]",
								listValue[j], type));
				}
			}

		} else
			throw new Exception("Not supported >= 3 dimensions");
	}

	public void iniValueForNotAssignedArrayVariable() {
		String nameVar = Utils.getNameOfArrayItem(rest);
		Variable parentVar = new Variable(nameVar, nameVar, type);
		parentVar.isArray = true;
		tableVar.add(parentVar);
		int dimension = Utils.getDimensionOfArrayItem(rest);
		if (dimension == 1) {
			String row = Utils.getRowOfOneDimensionArrayItem(rest);
			
			// neu khởi tạo mảng không có chỉ số. Ex: int a[];
			if (row == null || row.length() == 0) {
				
			} else
			// Nếu mảng có chỉ số hàng
			{
				int iniRow = Utils.isNumber(row) ? Utils.toInt(row)
						: staticVariable.defaultRowOfArray;
				for (int i = 0; i < iniRow; i++)
					parentVar.add(new Variable(nameVar + "[" + i + "]", nameVar
							+ "[" + i + "]", type));
			}
		} else if (dimension == 2) {
			String row = Utils.getRowOfTwoDimensionArrayItem(rest);
			String column = Utils.getColumnOfTwoDimensionArrayItem(rest);
			// neu khởi tạo mảng không có chỉ số. Ex1: int a[][3]; Ex2: int
			// a[][]; Ex1: int a[3][];
			if (row == null || column == null || row.length() == 0
					|| column.length() == 0) {
			} else
			// Nếu mảng có chỉ số hàng và cột
			{
				int rowIni = Utils.isNumber(row) ? Utils.toInt(row)
						: staticVariable.defaultRowOfArray;
				int colIni = Utils.isNumber(column) ? Utils.toInt(column)
						: staticVariable.defaultColumnOfArray;
				for (int i = 0; i < rowIni; i++)
					for (int j = 0; j < colIni; j++) {
						String nameIndex = null, valueIndex = null;
						nameIndex = valueIndex = nameVar + "[" + i + "][" + j
								+ "]";
						parentVar.add(new Variable(nameIndex, valueIndex, type));
					}
			}
		}		
	}

	@Override
	public String toString() {
		if (type == Variable.INT)
			return "type=INT, declaration=" + rest + "\n";
		else if (type == Variable.DOUBLE)
			return "type=DOUBLE, declaration=" + rest + "\n";
		return "type=OTHER, declaration=" + rest + "\n";
	}

	public static void main(String[] args) {
	}

}
