package generateTestCase;

import java.util.ArrayList;

public class ConditionNode {
	private String condition;
	private ArrayList<Variable> tableVar;

	/**
	 * 
	 * @param content
	 * @param tableVar
	 */
	public ConditionNode(String content, ArrayList<Variable> tableVar) {
		this.condition = content;
		this.tableVar = tableVar;
	}

	public String getOutput() {
		return condition;
	}

	public void run() throws Exception {
		condition = fillValue.fillNotEvaluate(condition, tableVar);
	}
}
