package generateTestCase;

// Hàm sinh random các giá trị số nguyên, số thực, kí tự, boolean

import java.text.DecimalFormat;
import java.util.Random;

public class GenerateRandom {

	public static int generateInteger(int canDuoi, int canTren) {
		if (canDuoi == canTren)
			return canDuoi;
		Random randomGenerator = new Random();
		if (canTren < 0)
			return -1
					* (randomGenerator.nextInt(-1 * canTren - (-1) * canDuoi) + (-1)
							* canDuoi);
		if (canDuoi >= 0)
			return randomGenerator.nextInt(canTren - canDuoi) + canDuoi;
		// canDuoi <0, canTren>0
		return randomGenerator.nextInt(canTren + (-1) * canDuoi) + canDuoi;
	}

	public static double generateDouble(int canDuoi, int canTren) {
		if (canDuoi == canTren) {
			return canDuoi;
		}
		int phanNguyen = generateInteger(canDuoi, canTren);
		Random rd = new Random();
		double phanThuc = rd.nextInt(99) / 100.0;
		DecimalFormat df = new DecimalFormat("#.##");
		phanThuc = Double.parseDouble(df.format(phanThuc));
		return Double.parseDouble(df.format(phanNguyen + phanThuc));
	}

	public static char generateCharRandom() {
		return 'a';
	}

	public static void main(String[] args) {

		// System.out.println(SinhRandom.SinhRandomKiTu());
		for (int i = 0; i <= 100; i++) {
			System.out.println(GenerateRandom.generateInteger(0, 10));
		}
		// System.out.println(SinhRandom.SinhRandomSoThuc(20));
	}
}
