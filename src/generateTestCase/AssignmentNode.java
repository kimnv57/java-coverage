package generateTestCase;

import java.util.ArrayList;

import java_coverage.Utils;

public class AssignmentNode {
	private String expression;
	private Variable updatedVariable;
	private ArrayList<Variable> tableVar;

	public AssignmentNode(String expression, ArrayList<Variable> tableVar) {
		this.expression = expression;
		this.tableVar = tableVar;
	}

	public Variable getVariable() {
		return updatedVariable;
	}

	private void updateVariable() {
		String vePhai = expression.split("=")[1];
		vePhai = fillValue.fill(vePhai, tableVar);
		String veTrai = expression.split("=")[0];
		// neu ve trai la bien mang
		if (Utils.isArrayItem(veTrai)) {
			String name = Utils.getNameOfArrayItem(veTrai);
			String index = Utils.getIndexOfArrayItem(veTrai);
			index = fillValue.fill(index, tableVar);
			veTrai = name + index;
		}
		int typeVeTrai = Utils.getTypeOfVariable(veTrai, tableVar);
		if (typeVeTrai == Variable.INT) {
			// Nếu vế phải là một số hoặc một hàm floor
			if (Utils.isInt(vePhai) || Utils.isFloorEquation(vePhai)) {
				// do nothing
			} else
				vePhai = new myJeval().evaluate("floor(" + vePhai + ")");
		}
		updatedVariable = Utils.updateVarInFatherTableVar(veTrai, vePhai,
				tableVar);
	}

	public void run() throws Exception {
		convertToTwoSide();
		updateVariable();
	}

	private void convertToTwoSide() {
		if (expression.contains("++")) {
			String nameVar = expression.replace("++", "");
			expression = nameVar + "=" + nameVar + "+ 1";

		} else if (expression.contains("*=") || expression.contains("+=")
				|| expression.contains("-=") || expression.contains("/=")) {
			String[] s = expression.split("=");
			expression = s[0].substring(0, s[0].length() - 1) + "="
					+ s[0].substring(0) + "(" + s[1] + ")";
		} else if (expression.contains("--")) {
			String nameVar = expression.replace("--", "");
			expression = nameVar + "=" + nameVar + "-1";
		}
	}

	public static void main(String[] args) throws Exception {
		String node="i++";
		ArrayList<Variable> tableVar1 = new ArrayList<Variable>();
		AssignmentNode an = new AssignmentNode(node, tableVar1);
		an.run();
	}
}
