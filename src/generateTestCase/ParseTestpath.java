package generateTestCase;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import java_coverage.Utils;
import java_coverage.staticVariable;

public class ParseTestpath {

	private String returnExpression;
	private String testpath;
	private String testcaseVariable;
	private String HPT;
	private ArrayList<Variable> tableVar;
	private Map<String, ArrayList<Variable>> hptMap;

	private String analysisProcessInString;

	public static void main(String[] args) throws Exception {
		String testpath1="(n > 1)#(sum+=1)#(n > 5)#(sum+=2)#(sum-=2)#(n > 10)#(sum+=3)#(a+=1)";
		String testVariable="int sum,int n,int a";
		String testpath = "(int low=0,high,mid)#(high=n-1)#(low<=high)#(mid=(low+high)/2)#(x<v[mid])#(high=mid-1)#!(low<=high)#(return -1)";
		String testcaseVariable = "int x,int v[],int n";
		// ,double *b,double c[3][3],double d[][3],double e[3],int n
		ParseTestpath p = new ParseTestpath(testpath1, testVariable);
		p.run();
		System.out.println("He phuong trinh:" + p.getHPT());
		System.out.println("Gia tri tra ve:" + p.getReturnExpression());
		System.out.println("Xau phan tich:" + p.getAnalysisProcessInString());
		System.out.println("Giai phuong trinh:");
		SolveEquation s = new SolveEquation(p.getHptMap(), testcaseVariable);
		s.setLoop(1000);
		staticVariable.setCanTren(20);
		staticVariable.setCanDuoi(0);
		s.run();
		System.out.println(s.getNghiemHpt());

	}

	/**
	 * Láº¥y báº£ng biáº¿n tÆ°Æ¡ng á»©ng vá»›i rÃ ng buá»™c
	 * 
	 * @param constraint
	 *            LÆ°u Ã½ constraint khÃ´ng cÃ²n biáº¿n thÆ°á»�ng Ä‘á»‹a
	 *            phÆ°Æ¡ng nÃ o. Chá»‰ cÃ³ biáº¿n testcase vÃ  biáº¿n máº£ng
	 *            Ä‘á»‹a phÆ°Æ¡ng
	 * @param tableVar
	 * @return
	 */
	private ArrayList<Variable> getVariableTableOfConstraint(String constraint,
			ArrayList<Variable> tableVar) {
		ArrayList<Variable> output = new ArrayList<Variable>();
		for (Variable item : tableVar)
			// Case: Náº¿u cÃ³ biáº¿n xuáº¥t hiá»‡n trong rÃ ng buá»™c
			// Action: PhÃ¢n tÃ­ch xem cÃ³ nÃªn thÃªm biáº¿n Ä‘Ã³ vÃ o báº£ng
			// biáº¿n
			// constraint khÃ´ng
			if (Pattern
					.compile("\\b" + Utils.getNameOfVariable(item.name) + "\\b")
					.matcher(constraint).find()) {
				// Case: Náº¿u lÃ  biáº¿n testcase vÃ  khÃ´ng pháº£i lÃ  máº£ng.
				// Action: Bá»� qua
				// Reason: ThÃªm vÃ o lÃ m cháº­t báº£ng; khÃ´ng cÃ³ Ã½ nghÄ©a
				// khi giáº£i há»‡
				// rÃ ng buá»™c
				if (item.isTestcaseVariable && item.isArray == false)
					continue;
				else
				// Case: biáº¿n nÃ y lÃ  biáº¿n máº£ng
				{
					Variable newItemOfTable = new Variable(item.name,
							item.value, item.type);
					newItemOfTable.isArray = item.isArray;
					newItemOfTable.isTestcaseVariable = item.isTestcaseVariable;
					if (item.containAvailableChildren())
						for (Variable v : item.getReferenceVars())
							// Case: Náº¿u biáº¿n cÃ³ giÃ¡ trá»‹ xÃ¡c Ä‘á»‹nh
							if (!v.name.equals(v.value))
								newItemOfTable.add(v.clone());
					output.add(newItemOfTable);
				}
			}
		return output;
	}

	public void run() throws Exception {
		int soBienTestcase = 0;
		// neu function, void co truyen tham so dau vao
		if (Utils.isAvailable(testcaseVariable))
			for (String item : testcaseVariable.split(",")) {
				testpath = "(" + item + ")#" + testpath;
				soBienTestcase++;
			}

		for (String node : testpath.split("#")) {
			node = Utils.convertCharacterToInt(node);
			System.out.println("chay: "+node+": "+getTypeOfTestpathItem(node));
			switch (getTypeOfTestpathItem(node)) {
			case DIEM_DIEU_KHIEN:
				ConditionNode cn = new ConditionNode(node, tableVar);
				cn.run();
				ArrayList<Variable> hptTableVar = getVariableTableOfConstraint(
						cn.getOutput(), tableVar);
				hptMap.put(cn.getOutput(), hptTableVar);
				//
				HPT += cn.getOutput() + "#";
				analysisProcessInString += cn.getOutput() + " :: tableVar="
						+ hptTableVar.toString() + "#";
				break;
			case GAN:
				node = node.substring(1, node.lastIndexOf(")"));
				AssignmentNode an = new AssignmentNode(node, tableVar);
				an.run();
				//
				analysisProcessInString += an.getVariable() + "#";
				break;
			case KHAI_BAO:
				int sizeBefore = tableVar.size();
				node = node.substring(1, node.lastIndexOf(")"));
				DelarationNode d = new DelarationNode(node, tableVar);
				d.run();
				//
				int sizeAfter = tableVar.size();
				if (soBienTestcase <= 0) {
					for (int i = sizeBefore; i < sizeAfter; i++)
						analysisProcessInString += tableVar.get(i).toString()
								+ ",";
					analysisProcessInString += "#";
				} else if (soBienTestcase == 1) {
					for (Variable v : tableVar)
						v.isTestcaseVariable = true;
				}
				soBienTestcase--;
				break;
			case RETURN:
				returnExpression = node.substring(1, node.lastIndexOf(")"))
						.replace("return ", "");
				returnExpression = fillValue.fill(returnExpression, tableVar);
				analysisProcessInString += "return " + returnExpression;
				break;
			case KHONG_XAC_DINH:
				analysisProcessInString += node + "#";
				break;
			}
		}
		HPT = HPT.contains("#") ? HPT.substring(0, HPT.lastIndexOf("#")) : "";
	}

	private int getTypeOfTestpathItem(String statement) {
		if (Pattern.compile("\\bprintf\\b").matcher(statement).find()
				|| Pattern.compile("\\bscanf\\b").matcher(statement).find()) {
			return KHONG_XAC_DINH;
		}
		if (statement.contains("==") || statement.contains(">")
				|| statement.contains("<") || statement.contains("!=")) {
			return DIEM_DIEU_KHIEN;
		}
		//
		String[] dataTypeIntegerList = { "char", "int", "short", "long" };
		String[] dataTypeDoubleList = { "double", "float" };
		String[] dataTypePre = { "const", "static", "unsigned", "signed" };
		for (String s : dataTypePre)
			if (statement.contains(s + " "))
				return KHAI_BAO;
		for (String s : dataTypeIntegerList) {
			if (statement.contains(s + " ")) {
				return KHAI_BAO;
			}
		}
		for (String s : dataTypeDoubleList) {
			if (statement.contains(s + " ")) {
				return KHAI_BAO;
			}
		}
		//
		if (statement.contains("++") || statement.contains("*=")
				|| statement.contains("/=") || statement.contains("+=")
				|| statement.contains("-=") || statement.contains("--")
				|| statement.contains("=")) {
			return GAN;
		}
		if (statement.contains("return" + " ")) {
			return RETURN;
		}
		return KHONG_XAC_DINH;
	}

	/**
	 * 
	 * @param testpath
	 *            testpath doesnot contains any unneccessary spaces
	 * @param testcaseVariable
	 *            testpath doesnot contains any unneccessary spaces
	 */
	public ParseTestpath(String testpath, String testcaseVariable) {
		this.testpath = testpath;
		returnExpression = HPT = analysisProcessInString = "";
		tableVar = new ArrayList<Variable>();
		hptMap = new LinkedHashMap<>();
		this.testcaseVariable = testcaseVariable;
		System.out.println(this.testcaseVariable);
		this.testcaseVariable = this.testcaseVariable.replaceAll("\\*(\\w+)",
				"$1[]");
		System.out.println(this.testcaseVariable);
		// this.testcaseVariable = this.testcaseVariable.replace("[]", "["
		// + staticVariable.defaultRowOfArray + "]");
	}

	public Map<String, ArrayList<Variable>> getHptMap() {
		return hptMap;
	}

	/**
	 * Get the return expression after parsing.
	 * 
	 * @return Return "" if the testpath doesnot contain any return expression.
	 */
	public String getReturnExpression() {
		return returnExpression;
	}

	/**
	 * 
	 * @return a string which decribe the analysis process the testpath in
	 *         detail. Ex: With testpath <br/>
	 *         !(a==b)#(int x = 0)#(a = b - 2)#((a==b)||(c==d))#(x = 1)#(return
	 *         1/x) <br/>
	 *         the output is: <br/>
	 *         !(a==b)#(x,0.0,type=INT),#(a,floor(b - 2),type=INT)#((floor(b -
	 *         2)==b)||(c==d))#(x,1.0,type=INT)#1.0
	 */
	public String getAnalysisProcessInString() {
		return analysisProcessInString;
	}

	/**
	 * 
	 * @return all equations in testpath after parsing. These equations is used
	 *         to generate testcase values in later phases.
	 */
	public String getHPT() {
		return HPT;
	}

	public ArrayList<Variable> getTableVar() {
		if (tableVar.size() == 0)
			return null;
		return tableVar;
	}

	private static final int DIEM_DIEU_KHIEN = 1;
	private static final int KHAI_BAO = 2;
	private static final int GAN = 3;
	private static final int KHONG_XAC_DINH = 4;
	private static final int RETURN = 5;
}
