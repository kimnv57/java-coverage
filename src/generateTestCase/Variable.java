package generateTestCase;

import java.util.ArrayList;

import variable.Helper;

public class Variable {
	public String name, value;
	public int type;
	public boolean isArray, isTestcaseVariable;
	private ArrayList<Variable> children;

	/**
	 * 
	 * @param nameVar
	 *            tÃªn Ä‘áº§y Ä‘á»§ cá»§a biáº¿n
	 * @return
	 */
	public boolean contain(String nameVar) {
		for (Variable v : getReferenceVars())
			if (v.equals(nameVar))
				return true;
		return false;
	}

	public Variable toLowerCase() {
		Variable v = this.clone();
		v.name = v.name.toLowerCase();
		v.value = v.value.toLowerCase();
		for (Variable item : v.getReferenceVars()) {
			item.name = item.name.toLowerCase();
			item.value = item.value.toLowerCase();
		}
		return v;
	}

	public ArrayList<Variable> getReferenceVars() {
		ArrayList<Variable> output = new ArrayList<Variable>();
		if (!isArray)
			output.add(this);
		else {
			if (containAvailableChildren())
				output = children;
		}
		return output;
	}

	public String replaceInString(String src) {
		// Náº¿u lÃ  biáº¿n máº£ng Ä‘Ã£ lÆ°u má»™t vÃ i pháº§n tá»­
		if (isArray) {
			if (containAvailableChildren())
				src = fillValue.replaceArrayVariable(src, children);
		} else
		// Náº¿u lÃ  biáº¿n thÆ°á»�ng
		{
			src = src.replaceAll("\\b" + name + "\\b", value);
		}
		return src;
	}

	private void iniSingleValue() throws Exception {
		switch (type) {
		case Variable.INT:
			value = "("
					+ GenerateRandom.generateInteger(
							Helper.canDuoi,
							Helper.canTren) + ")";
			break;
		case Variable.DOUBLE:
			value = "("
					+ GenerateRandom.generateDouble(Helper.canDuoi,
							Helper.canTren) + ")";
			break;
		default:
			throw new Exception(
					"Only handle with Integer and Float number (int,byte,float,double,...)");
		}
	}

	public void add(Variable v) {
		if (children == null)
			children = new ArrayList<Variable>();
		children.add(v);
	}

	public void iniValue() throws Exception {
		if (isArray) {
			if (containAvailableChildren())
				for (Variable v : children)
					v.iniSingleValue();
		} else
			iniSingleValue();
	}

	public boolean containAvailableChildren() {
		return children != null && children.size() > 0;
	}

	public Variable(String name, String value, int type,
			boolean isArrayVariable, boolean isTestcaseVariable) {
		this.name = name;
		this.value = value;
		this.type = type;
		isArray = isTestcaseVariable;
		this.isTestcaseVariable = false;
	}

	public Variable(String name, String value, int type) {
		this.name = name;
		this.value = value;
		this.type = type;
		isArray = false;
		this.isTestcaseVariable = false;
		//
		if (name.equals(value))
			this.value = value.toUpperCase();
	}

	public Variable() {
	}

	public Variable clone() {
		Variable v = new Variable(name, value, type);
		v.isArray = isArray;
		v.isTestcaseVariable = isTestcaseVariable;
		if (containAvailableChildren()) {
			for (Variable child : children)
				v.add(child.clone());
		}
		return v;
	}

	@Override
	public String toString() {
		if (children != null && children.size() > 0) {
			String outputString = "";
			outputString += "(" + name + ", array ::";
			for (Variable v : children)
				outputString += v.toString() + ", ";
			outputString += ")";
			return outputString;
		} else {
			if (type == DOUBLE)
				return "(" + name + "," + value + ",DOUBLE)";
			else
				return "(" + name + "," + value + ",INT)";
		}
	}

	public String toDisplayScreen() {
		return name+"="+value.substring(1,value.lastIndexOf(")"));
	}

	public static final int INT = 0;
	public static final int DOUBLE = 1;

	public void remove(Variable a) {
		children.remove(a);

	}
}