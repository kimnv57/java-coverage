package generateTestCase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java_coverage.Utils;

/**
 * The class is used to fill the real value of variables.
 * 
 * @author anhanh
 * 
 */
public class fillValue {

	/**
	 * Simplify the expression as much as possible.<br/>
	 * (a=1) or !(a==1) or ... but it must be surrounded in an bracket?????
	 */
	public static String fill(String expression, ArrayList<Variable> tableVar) {
		expression = fillNotEvaluate(expression, tableVar);
		expression = new myJeval().evaluate(expression);
		return expression;
	}

	public static String fillNotEvaluate(String expression,
			ArrayList<Variable> tableVar) {
		// a[n+a[m+2]]
		int maxLoop2 = expression.contains("[") ? 10 : 3;
		do {
			expression = replaceVariablesHoldSpecifiedValue(expression,
					tableVar);
			expression = simplifyInnestIndexOfArrayItem(expression);
			expression = simplifyFloatNumber(expression);
			maxLoop2--;
		} while (expression.contains("[") && maxLoop2 > 1);
		//
		expression = replaceVariablesHoldUnspecifiedValue(expression, tableVar);
		return expression;
	}

	public static String fill(String expression,
			ArrayList<Variable> testcaseTableVar,
			ArrayList<Variable> phuongTrinhTableVar) {
		expression = fillNotEvaluate(expression, testcaseTableVar,
				phuongTrinhTableVar);
		expression = new myJeval().evaluate(expression);
		return expression;
	}

	public static String fillNotEvaluate(String expression,
			ArrayList<Variable> testcaseTableVar,
			ArrayList<Variable> phuongTrinhTableVar) {
		int maxLoop2 = expression.contains("[") ? 10 : 3;
		do {
			expression = fillValue.replaceVariablesHoldSpecifiedValue(
					expression, testcaseTableVar);
			expression = fillValue.replaceVariablesHoldSpecifiedValue(
					expression, phuongTrinhTableVar);
			expression = fillValue.simplifyInnestIndexOfArrayItem(expression);
			expression = fillValue.simplifyFloatNumber(expression);
			maxLoop2--;
		} while (expression.contains("[") && maxLoop2 > 1);
		expression = replaceVariablesHoldUnspecifiedValue(expression,
				phuongTrinhTableVar);
		return expression;
	}

	/**
	 * 
	 * Replace all variables which its values arenot a specified number (might
	 * be array item... )with its values.
	 * 
	 * @param expression
	 * @param tableVar
	 * @return
	 */
	public static String replaceVariablesHoldUnspecifiedValue(
			String expression, ArrayList<Variable> tableVar) {
		if (!Utils.isAvailable(tableVar))
			return expression;
		for (Variable s : tableVar)
			for (Variable item : s.getReferenceVars())
				if (!Utils.isNumber(item.value))
					expression = expression.replaceAll(
							"\\b" + Utils.toRegex(item.name), item.value);
		return expression;
	}

	/**
	 * Replace all variables which its values are specified number with its
	 * values
	 * 
	 * @param expression
	 * @param tableVar
	 * @return
	 */
	public static String replaceVariablesHoldSpecifiedValue(String expression,
			ArrayList<Variable> tableVar) {
		if (!Utils.isAvailable(tableVar))
			return expression;
		for (Variable s : tableVar)
			for (Variable item : s.getReferenceVars())
				if (Utils.isNumber(item.value))
					expression = expression.replaceAll(
							"\\b" + Utils.toRegex(item.name), "(" + item.value
									+ ")");
		return expression;
	}

	/**
	 * Remove unneccessary ".0" String. Ex: 1.0 => 1<br/>
	 * 2.01+3.0=>2.01+3
	 * 
	 * @param expression
	 * @return
	 */
	public static String simplifyFloatNumber(String expression) {
		expression = expression + "@";// to simplify regex
		Matcher m = Pattern.compile("(\\d)\\.0([^\\d])").matcher(expression);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, m.group(1) + m.group(2));
		}
		m.appendTail(sb);
		return sb.deleteCharAt(sb.length() - 1).toString();
	}

	/**
	 * Calcalute the innest index of array item if can reduce. Ex: a[1+2] =>
	 * a[3] <br/>
	 * a[1/2] => a[0] a[1+a[3+1]] =>a[1+a[4]]
	 * 
	 * @param expression
	 * @return
	 */
	public static String simplifyInnestIndexOfArrayItem(String expression) {
		expression = expression + "@";// to simplify regex
		Matcher m = Pattern.compile("\\[([^\\]\\[]+)\\]").matcher(expression);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, "[" + new myJeval().evaluate(m.group(1))
					+ "]");
		}
		m.appendTail(sb);
		return sb.deleteCharAt(sb.length() - 1).toString();
	}

	/**
	 * Replace array variables with its values.
	 * 
	 * @param expression
	 * @param tableVar
	 * @return
	 */
	public static String replaceArrayVariable(String expression,
			ArrayList<Variable> tableVar) {
		if (!Utils.isAvailable(tableVar))
			return expression;
		for (Variable s : tableVar)
			if (s.isArray)
				for (Variable item : s.getReferenceVars())
					expression = expression.replaceAll(
							"\\b" + Utils.toRegex(item.name), "(" + item.value
									+ ")");
		return expression;
	}

	/**
	 * Replace all simple variables (not array variables) with its values
	 * 
	 * @param expression
	 * @param tableVar
	 * @return
	 */
	public static String replaceSimpleVariable(String expression,
			ArrayList<Variable> tableVar) {
		if (!Utils.isAvailable(tableVar))
			return expression;
		for (Variable item : tableVar)
			if (!item.isArray)
				expression = expression.replaceAll(
						"\\b" + Utils.toRegex(item.name), "(" + item.value
								+ ")");
		return expression;
	}

}
