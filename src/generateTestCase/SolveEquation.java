package generateTestCase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java_coverage.Utils;

public class SolveEquation {

	private int numLoop;
	private String realOutput;
	private String nghiem;
	private String nghiemSimple;
	private Map<String, ArrayList<Variable>> hptMap;
	private String testcaseVariable;
	private ArrayList<Variable> testcaseTableVar;

	public static void main(String[] args) throws Exception {
	}

	private Map<String, ArrayList<Variable>> getCopyOfhptMap() {
		Map<String, ArrayList<Variable>> copy = new LinkedHashMap<String, ArrayList<Variable>>();
		for (String key : hptMap.keySet()) {
			ArrayList<Variable> a = new ArrayList<>();
			for (Variable v : hptMap.get(key))
				a.add(v.clone());
			copy.put(key, a);
		}
		return copy;
	}

	private ArrayList<Variable> getCopyOftestcaseTableVar() {
		ArrayList<Variable> copy = new ArrayList<Variable>();
		for (Variable v : testcaseTableVar)
			copy.add(v.clone());
		return copy;
	}

	private boolean isLuonLuonVoNghiem() {
		Map<String, ArrayList<Variable>> hptMapCopy = getCopyOfhptMap();
		boolean luonLuonVoNghiem = false;
		for (String condition : hptMapCopy.keySet()) {
			String kq = new myJeval().evaluate(condition);
			if (kq.equals("0") || kq.equals("0.0")) {
				luonLuonVoNghiem = true;
				break;
			} else if (kq.equals("1") || kq.equals("1.0")) {
				luonLuonVoNghiem = false;
				hptMap.remove(condition);
			}
		}
		hptMap = hptMapCopy;
		return luonLuonVoNghiem;
	}

	private void getAndIniALLInnestArrayItem(String equation,
			ArrayList<Variable> tableVar) throws Exception {
		Matcher m = Pattern.compile("([a-zA-Z0-9]+)(\\[([^\\]\\[]+)\\])+")
				.matcher(equation);
		//
		Set<String> listVar = new HashSet<>();
		while (m.find())
			listVar.add(m.group(0) + ","
					+ Utils.getTypeOfVariable(m.group(0), tableVar));
		//
		for (String item : listVar) {
			String name = item.split(",")[0];
			int type = Utils.toInt(item.split(",")[1]);
			Variable parent = Utils.getParentTableVar(name, tableVar);
			Variable existVar = Utils.getVariableInParentTableVar(name,
					parent.getReferenceVars());
			// Náº¿u biáº¿n chÆ°a cÃ³ trong báº£ng biáº¿n cha
			if (existVar == null) {
				Variable newVar = new Variable(name, name, type);
				newVar.iniValue();
				parent.add(newVar);
				break;
			}
		}
	}

	/**
	 * Vá»«a rÃºt gá»�n rÃ ng buá»™c, vá»«a tÃ¬m thÃªm nhá»¯ng biáº¿n máº£ng má»›i vÃ  táº¡o giÃ¡ trá»‹
	 * cho nÃ³.
	 * 
	 * @param constraint
	 * @param testcaseVarTable
	 * @param constraintVarTable
	 * @return
	 * @throws Exception
	 */
	private String replaceInCondition(String constraint,
			ArrayList<Variable> testcaseVarTable,
			ArrayList<Variable> constraintVarTable) throws Exception {
		int dem = 5;
		do {
			// int before = constraint.length();
			// RÃºt gá»�n constraint háº¿t má»©c cÃ³ thá»ƒ
			constraint = fillValue.fill(constraint, testcaseVarTable,
					constraintVarTable);
			int after = constraint.length();
			// if (before == after)
			// break;
			// Láº¥y nhá»¯ng biáº¿n máº£ng trong cÃ¹ng vÃ  táº¡o giÃ¡ trá»‹ cho nÃ³
			getAndIniALLInnestArrayItem(constraint, constraintVarTable);
			dem--;

		} while (constraint.contains("[") && dem >= 0);
		return constraint;
	}

	public ArrayList<Variable> rutGonBangBien(
			ArrayList<Variable> bangBienCanRutGon,
			ArrayList<Variable> testcaseTableVar) {
		for (Variable item : bangBienCanRutGon)
			if (item.containAvailableChildren()) {
				for (int i = 0; i < item.getReferenceVars().size() - 1; i++) {
					Variable v = item.getReferenceVars().get(i);
					v.value = fillValue.fill(v.value, testcaseTableVar);
					v.name = fillValue.fill(v.name, testcaseTableVar);
					// do later: Náº¿u item cÃ³ má»™t biáº¿n máº£ng tÃªn y há»‡t náº±m vá»‹ trÃ­
					// sau; ta loáº¡i vá»‹ trÃ­ hiá»‡n táº¡i khá»�i báº£ng biáº¿n
					for (int j = i + 1; j < item.getReferenceVars().size(); j++) {
						Variable vv = item.getReferenceVars().get(j);
						if (vv.name.equals(v.name)) {
							item.getReferenceVars().remove(v);
							i--;
							break;
						}
					}
				}
			}
		return bangBienCanRutGon;
	}

	public void run() throws Exception {
		// neu function,void khong co tham so truyen vao; hoac pt luon vo nghiem
		if (testcaseVariable == null || testcaseVariable.length() == 0
				|| isLuonLuonVoNghiem())
			return;
		//

		testcaseTableVar = createTableVar(testcaseVariable);
		int[] traceUnsolveEquation = new int[hptMap.size()];
		do {
			ArrayList<Variable> testcaseTableVarCopy = getCopyOftestcaseTableVar();
			Map<String, ArrayList<Variable>> hptMapCopy = getCopyOfhptMap();
			iniTestcaseTableVar(testcaseTableVarCopy);
			// Set <String> tapBienMang = new HashSet();
			//
			int dem = 0;
			for (String constraint : hptMapCopy.keySet()) {
				// Báº£ng biáº¿n constraint cÃ³ thá»ƒ cÃ³ nhá»¯ng biáº¿n máº£ng khÃ¡c nhau;
				// nhÆ°ng thay giÃ¡ trá»‹ vÃ o thÃ¬ trÃ¹ng nhau
				ArrayList<Variable> bangBienConstraint = rutGonBangBien(
						hptMapCopy.get(constraint), testcaseTableVarCopy);
				constraint = replaceInCondition(constraint,
						testcaseTableVarCopy, bangBienConstraint);
				constraint = new myJeval().evaluate(constraint);
				// neu bo gia tri testcase thoa man dieu kien i
				if (constraint.equals("1.0") || constraint.equals("1")) {
					traceUnsolveEquation[dem]++;
					dem++;
				} else
				// neu bo gia tri testcase KHONG thoa man dieu kien i
				{
					break;
				}
			}
			// neu bo gia tri testcase thoa man tat ca dieu kien
			if (dem == hptMapCopy.size()) {
				nghiem = setNghiem(testcaseTableVarCopy);
				Map<String, String> tapBienMang = new LinkedHashMap<>();
				for (String constraint : hptMapCopy.keySet())
					for (Variable item : hptMapCopy.get(constraint))
						if (item.isTestcaseVariable)
							for (Variable v : item.getReferenceVars())
								tapBienMang.put(v.name, v.value);
				nghiem += tapBienMang.toString();
				realOutput = fillValue.fill(realOutput, testcaseTableVarCopy);
				break;
			} else
				// neu bo gia tri testcase KHONG thoa man tat ca dieu kien
				numLoop--;
			// Neu so lan lap van chua het
		} while (numLoop > 0);
		// do later: tinh RO
		if (numLoop == 0)
			for (int i = 0; i < traceUnsolveEquation.length; i++)
				if (traceUnsolveEquation[i] == 0) {
					nghiem = "Equation " + i + " : unsolved!";
					break;
				}
	}

	/**
	 * Lay danh sach (dang table) chua cac bien testcase
	 * 
	 * @param testcaseVariable
	 * @return
	 * @throws Exception
	 */
	private ArrayList<Variable> createTableVar(String testcaseVariable)
			throws Exception {
		ParseTestpath p = new ParseTestpath("", testcaseVariable);
		p.run();
		testcaseTableVar = new ArrayList<Variable>();
		Set<String> arraySet = new HashSet<>();
		for (Variable v : p.getTableVar())
			// Náº¿u khÃ´ng pháº£i biáº¿n máº£ng thÃ¬ lÆ°u luÃ´n biáº¿n Ä‘Ã³
			if (!Utils.isArrayItem(v.name))
				testcaseTableVar.add(v);
			else
				// Náº¿u lÃ  biáº¿n máº£ng thÃ¬ chá»‰ lÆ°u tÃªn vÃ  kiá»ƒu biáº¿n máº£ng Ä‘Ã³
				arraySet.add(v.name + "," + v.type);
		for (String itemString : arraySet) {
			Variable v = new Variable(itemString.split(",")[0],
					itemString.split(",")[0],
					Utils.toInt(itemString.split(",")[1]));
			v.isArray = true;
			testcaseTableVar.add(v);
		}
		return testcaseTableVar;
	}

	private void iniTestcaseTableVar(ArrayList<Variable> tableVar)
			throws Exception {
		for (Variable v : tableVar)
			v.iniValue();
	}

	private String setNghiem(ArrayList<Variable> testcaseTableVar) {
		String nghiem = "";
		for (Variable v : testcaseTableVar)
			if (!Utils.isArrayItem(v.name))
				nghiem += v.toDisplayScreen() + " ;";
		for (Variable v : testcaseTableVar)
			if (Utils.isArrayItem(v.name))
				nghiem += v.toDisplayScreen() + " ;";
		return nghiem;

	}

	public SolveEquation(Map<String, ArrayList<Variable>> hptMap,
			String testcase) {
		this.hptMap = hptMap;
		this.testcaseVariable = testcase;
		nghiem = "-";
		numLoop = 0;
		realOutput = "";
		//
		this.hptMap = convertToLowerCase();
	}

	private Map<String, ArrayList<Variable>> convertToLowerCase() {
		Map<String, ArrayList<Variable>> copy = new LinkedHashMap<String, ArrayList<Variable>>();
		for (String key : hptMap.keySet()) {
			ArrayList<Variable> a = new ArrayList<>();
			for (Variable v : hptMap.get(key))
				a.add(v.toLowerCase());
			copy.put(key.toLowerCase(), a);
		}
		return copy;
	}

	public void setLoop(int i) {
		numLoop = i;
	}

	public void setReturnValue(String s) {
		realOutput = s;
	}

	public String getNghiemHpt() {
		return nghiem;
	}

	public String getReturnValue() {
		return realOutput;
	}

}
