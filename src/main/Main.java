package main;

import generateTestCase.ParseTestpath;
import generateTestCase.SolveEquation;

import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import java_coverage.staticVariable;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import variable.Helper;
import draw_graphics.Canvas;

public class Main {
	private JMenuItem mnOpen;
	private JFrame frmResult;
	JFileChooser openfile;
	private JScrollPane scrollPane_1;

	private Canvas mainPanel;
	private JTextPane textPaneSource;
	private JTextPane textPanePath;
	private JTable StatementTable;
	private JTextField variableBound;
	private JButton btRandomTestCase;
	private JTable BranchTable;

	public void alert(Object str) {
		javax.swing.JOptionPane.showMessageDialog(null, str);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmResult.setVisible(true);
					window.frmResult.setExtendedState(JFrame.MAXIMIZED_BOTH);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		initialize();

		setListeners();

	}

	private void setListeners() {
		mnOpen.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				System.out.println("click" + arg0.getActionCommand());

				openfile = new JFileChooser("E://filenckh");

				int returnVal = openfile.showOpenDialog(frmResult);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = openfile.getSelectedFile();
					System.out.println("Opening: " + file.getPath());

					try {
						Coverage Program = new Coverage(file.getPath());
						mainPanel.createGraphFromNodeList(Helper.NodeRelations12);
						textPaneSource.setText(Helper.getSourceMethod());
						textPanePath.setText(Helper.NodeRelations12);
						displayBasicsPath();
						// mainPanel.showSttSelectedTestpath("((a <= 0) || (b <= 0) || (c <= 0))#(((a + b) <= c) || ((a + c) <= b) || ((b + c) <= a))#(return 0)");
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					System.out.println("Open command cancelled by user.");
				}
			}

		});
		btRandomTestCase.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					String bound= variableBound.getText();
					String bounds[]=bound.split(",");
					Helper.canDuoi=Integer.valueOf(bounds[0]);
					Helper.canTren=Integer.valueOf(bounds[1]);
					randomTestCase();
					displayBasicsPath2();
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		});

		StatementTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent event) {
						int row = StatementTable.getSelectedRow();
						if (row < Helper.BasisPathsIndex.split("\n").length
								&& row >= 0) {
							mainPanel
									.showSttSelectedTestpath(Helper.BasisPathsLuanLy
											.split("\n")[row]);
							mainPanel.updateLineColorWhenClick(Helper.BasisPathsLuanLy.split("\n")[row]);
						}
					}
				});

	}

	protected void randomTestCase() throws Exception {
		String []result = findTestcase(Helper.BasisPathsLuanLy);
		Helper.RO=result[0];
		Helper.TC=result[1];
		System.out.println("Helper.RO: \n"+Helper.RO);
	}
	public String[] findTestcase(String TPList) throws Exception {
		String[] output = new String[4];
		for (int i = 0; i < output.length; i++) {
			output[i] = "";
		}
		String[] tp1Split = TPList.split("\n");
		String RO, TC;
		RO = TC = new String();
		for (String testpath : tp1Split) {
			ParseTestpath p = new ParseTestpath(testpath,
					Helper.variableOfMethod);
			p.run();
			String returnExpression = p.getReturnExpression();
			SolveEquation f = new SolveEquation(p.getHptMap(),
					Helper.variableOfMethod);
			System.out.println("hpt:"+p.getHPT() + "\n" +Helper.variableOfMethod
					+ "\n" + returnExpression);
			f.setLoop(staticVariable.getMaxLoop());
			f.setReturnValue(returnExpression);
			f.run();
			TC += f.getNghiemHpt() + "\n";
			RO += f.getReturnValue() + "\n";
			System.out.println(f.getNghiemHpt());
			output[2] += p.getAnalysisProcessInString() + "\n";
			output[3] += p.getHPT() + "\n";
		}
		output[0] = RO;
		output[1] = TC;
		return output;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmResult = new JFrame();
		frmResult.setTitle("Result");
		frmResult.setBounds(10, 10, 1200, 600);
		frmResult.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		scrollPane_1 = new JScrollPane();

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		JButton btnOpen = new JButton("Open");

		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		
		JLabel lblab = new JLabel("[a,b]");
		
		variableBound = new JTextField();
		variableBound.setText("-10,10");
		variableBound.setColumns(10);
		
		btRandomTestCase = new JButton("random test case");
		GroupLayout groupLayout = new GroupLayout(frmResult.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(22)
							.addComponent(lblab)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(variableBound, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btRandomTestCase)
							.addGap(854)
							.addComponent(btnOpen)
							.addGap(30))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(tabbedPane_1, GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
								.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 537, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)))
					.addGap(2))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnOpen)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblab)
							.addComponent(variableBound, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(btRandomTestCase)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(tabbedPane_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE))
					.addGap(0))
		);

		JPanel panel_1 = new JPanel();
		tabbedPane_1.addTab("Statement", null, panel_1, null);

		JScrollPane scrollPane_3 = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(gl_panel_1.createParallelGroup(
				Alignment.LEADING).addComponent(scrollPane_3,
				GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE));
		gl_panel_1.setVerticalGroup(gl_panel_1.createParallelGroup(
				Alignment.LEADING).addComponent(scrollPane_3,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 277,
				Short.MAX_VALUE));

		StatementTable = new JTable();

		

		scrollPane_3.setViewportView(StatementTable);

		panel_1.setLayout(gl_panel_1);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		tabbedPane_1.addTab("Branch", null, scrollPane_4, null);
		
		BranchTable = new JTable();
		scrollPane_4.setViewportView(BranchTable);
		setTableDefault();

		mainPanel = new Canvas();
		scrollPane_1.setViewportView(mainPanel);

		JPanel panel = new JPanel();
		tabbedPane.addTab("source code", null, panel, null);
		panel.setLayout(new CardLayout(0, 0));

		JScrollPane scrollPane_2 = new JScrollPane();
		panel.add(scrollPane_2, "name_339913629530375");

		textPaneSource = new JTextPane();
		scrollPane_2.setViewportView(textPaneSource);

		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Path", null, panel_3, null);

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(gl_panel_3.createParallelGroup(
				Alignment.LEADING).addComponent(scrollPane, Alignment.TRAILING,
				GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE));
		gl_panel_3.setVerticalGroup(gl_panel_3.createParallelGroup(
				Alignment.LEADING).addComponent(scrollPane,
				GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE));

		textPanePath = new JTextPane();
		scrollPane.setViewportView(textPanePath);
		panel_3.setLayout(gl_panel_3);

		frmResult.getContentPane().setLayout(groupLayout);

		JMenuBar menuBar = new JMenuBar();
		frmResult.setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("file");
		menuBar.add(mnNewMenu);

		mnOpen = new JMenuItem("open");
		mnNewMenu.add(mnOpen);

		JMenuItem mntmExit = new JMenuItem("exit");
		mnNewMenu.add(mntmExit);

	}

	private void displayBasicsPath() {
		DefaultTableModel tm = (DefaultTableModel) StatementTable.getModel();
		int numRow = Helper.BasisPathsIndex.split("\n").length;
		for (int row = 0; row < numRow; row++) {
			String testpath = Helper.BasisPathsLuanLy.split("\n")[row];
			String hpt = Helper.hpt2.split("\n")[row], TC = "", EO = "", RO = "", Result = "";
			updateRow(row, new String[] { row + "", testpath, hpt, TC, EO, RO,
					Result }, tm);
		}
	}
	private void displayBasicsPath2() {
		DefaultTableModel tm = (DefaultTableModel) StatementTable.getModel();
		int numRow = Helper.BasisPathsIndex.split("\n").length;
		for (int row = 0; row < numRow; row++) {
			String testpath = Helper.BasisPathsLuanLy.split("\n")[row];
			String hpt = Helper.hpt2.split("\n")[row], TC = Helper.TC.split("\n")[row],
			EO = "", RO = Helper.RO.split("\n")[row], Result = "";
			updateRow(row, new String[] { row + "", testpath, hpt, TC, EO, RO,
					Result }, tm);
		}
	}

	private void updateRow(int row, String[] o, DefaultTableModel tm) {
		for (int colum = 0; colum < o.length; colum++) {
			tm.setValueAt(o[colum], row, colum);
		}
	}

	private void setTableDefault() {
		StatementTable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {

				}, new String[] { "STT", "Path", "Equations", "Test case",
						"EO", "RO", "Result" }) {
			Class[] types = new Class[] { java.lang.Integer.class,
					java.lang.String.class, java.lang.String.class,
					java.lang.String.class, java.lang.String.class,
					java.lang.String.class, java.lang.String.class };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}
		});
		StatementTable.getTableHeader().setReorderingAllowed(false);

		String[] o = new String[Helper.NUMBER_COLUM_PATH];
		for (int i = 0; i < o.length; i++) {
			o[i] = "";
		}
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			((DefaultTableModel) StatementTable.getModel()).addRow(o);
		}
		if (StatementTable.getColumnModel().getColumnCount() > 0) {
			StatementTable.getColumnModel().getColumn(0).setMinWidth(0);
			StatementTable.getColumnModel().getColumn(0).setMaxWidth(40);
		}
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			((DefaultTableModel) BranchTable.getModel()).addRow(o);
		}
		if (BranchTable.getColumnModel().getColumnCount() > 0) {
			BranchTable.getColumnModel().getColumn(0).setMinWidth(0);
			BranchTable.getColumnModel().getColumn(0).setMaxWidth(40);
		}
		
		
		//
		BranchTable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {

				}, new String[] { "STT", "Path", "Equations", "Test case",
						"EO", "RO", "Result" }) {
			Class[] types = new Class[] { java.lang.Integer.class,
					java.lang.String.class, java.lang.String.class,
					java.lang.String.class, java.lang.String.class,
					java.lang.String.class, java.lang.String.class };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}
		});
		BranchTable.getTableHeader().setReorderingAllowed(false);
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			((DefaultTableModel) BranchTable.getModel()).addRow(o);
		}
		if (BranchTable.getColumnModel().getColumnCount() > 0) {
			BranchTable.getColumnModel().getColumn(0).setMinWidth(0);
			BranchTable.getColumnModel().getColumn(0).setMaxWidth(40);
		}
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			((DefaultTableModel) BranchTable.getModel()).addRow(o);
		}
		if (BranchTable.getColumnModel().getColumnCount() > 0) {
			BranchTable.getColumnModel().getColumn(0).setMinWidth(0);
			BranchTable.getColumnModel().getColumn(0).setMaxWidth(40);
		}
	}
}
