package main;

public class JNode {
	final public static int NORMAL_NODE = 0;
	final public static int IF_NODE = 1;
	final public static int FOR_NODE = 2;
	final public static int WHILE_NODE = 3;
	private int NodeId;
	private String Content;
	private int trueNode;
	private int falseNode;
	private int Type;

	public JNode() {

	}

	public JNode(int id, int trueNode, int falseNode, String content,
			int nodeType) {
		NodeId = id;
		if (content.indexOf("\n") != -1) {
			Content = content.substring(0, content.indexOf("\n"));
		} else
			Content = content;

		this.trueNode = trueNode;
		this.falseNode = falseNode;
		Type = nodeType;
	}

	public void setTrueNode(int t) {
		trueNode = t;
	}

	public int getTrueNode() {
		return trueNode;
	}

	public void setFalseNode(int t) {
		falseNode = t;
	}

	public int getFalseNode() {
		return falseNode;
	}

	public void setNodeId(int id) {
		NodeId = id;
	}

	public int getFalseId() {
		return NodeId;
	}

	public void setNodeContent(String c) {
		Content = c;
	}

	public String getNodeContent() {
		return Content;
	}

	public void setId(int id) {
		NodeId = id;
	}

	public int getId() {
		return NodeId;
	}

	public String toString() {

		return String.format("%d#%s#true=%d#false=%d", NodeId, Content,
				trueNode, falseNode);
	}

}
