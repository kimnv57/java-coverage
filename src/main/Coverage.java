package main;

import generateTestCase.ParseTestpath;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import java_coverage.GetGraph;
import java_coverage.findBasisPaths;
import variable.Helper;

public class Coverage {
	ArrayList<JNode> NodeArray;

	public Coverage(String filePath) throws Exception {
		NodeArray = new ArrayList<JNode>();
		CreateASTTreeFromFilePath(filePath);
		getPathData(Helper.dsk3, Helper.NodeElements12, 0);
		getPathData(Helper.dsk3, Helper.NodeElements12, 1);
		getPathData(Helper.dsk3, Helper.NodeElements12, 2);

	}

	protected void CreateASTTreeFromFilePath(String path) {
		try {
			String source = "";
			File myFile = new File(path);
			Scanner x = new Scanner(myFile);
			while (x.hasNext()) {
				String currentText = x.nextLine();
				source += currentText + "\n";
			}
			char[] sourceChar = source.toCharArray();
			NodeArray = new GetGraph(sourceChar).getNodeArray();
			String path1 = "";
			for (int i = 0; i < NodeArray.size(); i++) {
				System.out.println(NodeArray.get(i));
				path1 += NodeArray.get(i) + "\n";
			}
			// create static variable
			Helper.NodeRelations12 = path1;
			Helper.dsk12 = createDsk12();
			Helper.dsk3 = createDsk3();
			Helper.NodeElements12 = createNodeElements12();
			String sourceMethod = Helper.getSourceMethod();
			Helper.variableOfMethod = sourceMethod.substring(
					sourceMethod.indexOf("(") + 1, sourceMethod.indexOf(")"));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String createNodeElements12() {
		String result = "";
		for (int i = 0; i < NodeArray.size(); i++) {

			JNode node = NodeArray.get(i);
			result += node.getId() + "#" + node.getNodeContent().trim() + "\n";
		}

		result = result.replace(";", "");
		return result;
	}

	private String createDsk3() {
		String dsk3 = "-1 0 0\n";
		for (int i = 0; i < NodeArray.size(); i++) {

			JNode node = NodeArray.get(i);
			dsk3 += node.getId() + " " + node.getTrueNode() + " "
					+ node.getFalseNode() + "\n";
		}

		return dsk3;
	}

	private String createDsk12() {
		String dsk12 = "-1 0\n";
		for (int i = 0; i < NodeArray.size(); i++) {

			JNode node = NodeArray.get(i);
			if (node.getFalseNode() == node.getTrueNode()) {
				dsk12 += node.getId() + " " + node.getTrueNode() + "\n";
			} else {
				dsk12 += node.getId() + " " + node.getTrueNode() + " "
						+ node.getFalseNode() + "\n";
			}
		}

		return dsk12;
	}

	public static void main(String[] args) throws Exception {
		Coverage aa = new Coverage("E:\\filenckh\\sum.java");
		System.out.println(Helper.dsk12);
		System.out.println(Helper.dsk3);
		System.out.println(Helper.NodeElements12);
		System.out.println(Helper.BasisPathsLuanLy);
		System.out.println("hpt1: " + Helper.hpt1);
	}

	private void getPathData(String Dsk, String nodeElement, int levelCoverage)
			throws Exception {
		findBasisPaths.findBasisPaths(Dsk);
		System.out.println("basic path:"
				+ findBasisPaths.getBasisPaths().toString());
		Map<String, String[]> dsk = getDskObject(Dsk);
		Map<String, String> nodeElementsMap = getNodeObject(nodeElement);
		Set nodeList = new HashSet();
		for (ArrayList<Integer> ai : findBasisPaths.getBasisPaths()) {
			String newTP = getTPLuanLyFromIndex(ai, dsk, nodeElementsMap);
			System.out.println("newtp:"+newTP);
			ParseTestpath p = new ParseTestpath(newTP, Helper.variableOfMethod);
			p.run();

			System.out.println("He phuong trinh:" + p.getHPT());
			System.out.println("Gia tri tra ve:" + p.getReturnExpression());
			System.out.println("Xau phan tich:"
					+ p.getAnalysisProcessInString());
			int sizeBefore = nodeList.size();
			switch (levelCoverage) {
			case 0:
				Helper.BasisPathsLuanLy += newTP + "\n";
				for (int k = 1; k < ai.size() - 1; k++) {
					Helper.BasisPathsIndex += ai.get(k) + " ";
				}
				Helper.BasisPathsIndex += "\n";
				// Helper.hptBasisPaths += p.getHPT() + "\n";
				// Helper.analysisProcessInStringBasisPaths += p
				// .getAnalysisProcessInString() + "\n";
				// Helper.CC = findBasisPaths.getCC();
				break;

			case 1:
				for (int item : ai) {
					nodeList.add(item);
				}
				if (sizeBefore != nodeList.size()) {
					Helper.TPLuanLy1 += newTP + "\n";
					for (int k = 1; k < ai.size() - 1; k++) {
						Helper.TPIndex1 += ai.get(k) + " ";
					}
					Helper.TPIndex1 += "\n";
					Helper.hpt1 += p.getHPT() + "\n";
					Helper.analysisProcessInString1 += p
							.getAnalysisProcessInString() + "\n";
				}
				break;

			case 2:
				for (int i = 0; i < ai.size() - 1; i++) {
					nodeList.add(ai.get(i) + "->" + ai.get(i + 1));
				}
				if (sizeBefore != nodeList.size()) {
					Helper.TPLuanLy2 += newTP + "\n";
					for (int k = 1; k < ai.size() - 1; k++) {
						Helper.TPIndex2 += ai.get(k) + " ";
					}
					Helper.TPIndex2 += "\n";
					Helper.hpt2 += p.getHPT() + "\n";
					Helper.analysisProcessInString2 += p
							.getAnalysisProcessInString() + "\n";
				}
				break;
			}
		}
		System.out.println(Helper.dsk12);
		System.out.println(Helper.dsk3);
		System.out.println(Helper.NodeElements12);
		System.out.println(Helper.BasisPathsLuanLy);
	}

	private String getTPLuanLyFromIndex(ArrayList<Integer> indexList,
			Map<String, String[]> dsk, Map<String, String> nodeElementsMap) {
		String output = new String();
		for (int iCount = 1; iCount < indexList.size() - 1; iCount++) {
			int idCurrent = indexList.get(iCount);
			int idNext = indexList.get(iCount + 1);
			for (Object o : dsk.keySet()) {
				String key = o.toString();
				String[] value = dsk.get(o);
				if (key.equals(idCurrent + "")) {
					if (value[0].equals(idNext + "")) {
						output += "(" + nodeElementsMap.get(idCurrent + "")
								+ ")#";
						break;
					} else if (value[1].equals(idNext + "")) {
						output += "!(" + nodeElementsMap.get(idCurrent + "")
								+ ")#";
						break;
					}
				}
			}
		}
		return output.substring(0, output.lastIndexOf("#"));
	}

	private Map<String, String> getNodeObject(String nodeelement) {
		Map<String, String> nodeElementsMap = new HashMap<>();
		for (String s : nodeelement.split("\n")) {
			nodeElementsMap.put(s.split("#")[0], s.split("#")[1]);
		}
		return nodeElementsMap;
	}

	private Map<String, String[]> getDskObject(String danhsachke) {
		Map<String, String[]> dsk = new HashMap<>();
		for (String s : danhsachke.split("\n")) {
			String[] itemList = s.split(" ");
			switch (itemList.length) {
			case 3:
				if (itemList[1].equals(itemList[2])) {
					dsk.put(itemList[0], new String[] { itemList[1] });
				} else {
					dsk.put(itemList[0], new String[] { itemList[1],
							itemList[2] });
				}
				break;
			case 2:
				dsk.put(itemList[0], new String[] { itemList[1] });
				break;
			}
		}
		return dsk;
	}

}
